import { Module } from '@nestjs/common';
import { AuthorModule } from './author/author.module'
import { GraphQLModule } from '@nestjs/graphql';
import { AppController } from './app.controller';
import { AppService } from './app.service';

@Module({
  imports: [
    AuthorModule,
    GraphQLModule.forRoot({
      autoSchemaFile: './graphql/schema.gql'
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
